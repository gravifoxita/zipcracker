# Create by gravifoxita #

# Import necessary libraries
import zipfile

# Define the file path and password list
file_path = "example.zip"
passwords = <"password1", "password2", "password3">

# Loop through password list and try each password to extract the zip file
for password in passwords:
    try:
        with zipfile.ZipFile(file_path) as zf:
            zf.extractall(pwd=bytes(password, 'utf-8'))
        print("Success! Password is:", password)
        break
    except:
        pass

# If none of the passwords work, print a failure message
if not password:
    print("Unable to crack password. Try a different password list.")